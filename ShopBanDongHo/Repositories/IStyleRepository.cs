﻿using ShopBanDongHo.Models;

namespace ShopBanDongHo.Repositories
{
	public interface IStyleRepository
	{
		Task<IEnumerable<Style>> GetAllAsync();
		Task<Style> GetByIdAsync(int id);
		Task AddAsync(Style style);
		Task UpdateAsync(Style style);
		Task DeleteAsync(int id);
	}
}
