﻿using Microsoft.EntityFrameworkCore;
using ShopBanDongHo.Models;

namespace ShopBanDongHo.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>
        options) : base(options)
        {
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<Brand> Brands { get; set; }

        public DbSet<Style> Styles { get; set; }

    }
}
