﻿using ShopBanDongHo.Models;
using ShopBanDongHo.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;


namespace ShopBanDongHo.Controllers
{
    public class StyleController : Controller
    {
        private readonly IStyleRepository _styleRepository; 

        public StyleController(IStyleRepository styleRepository)
        {
            _styleRepository = styleRepository;
        }

        // Hiển thị danh sách sản phẩm
        
        public async Task<IActionResult> Index()
        {
            var styles = await _styleRepository.GetAllAsync();
            return View(styles);
        }
        // Hiển thị form thêm sản phẩm mới

        public async Task<IActionResult> Add()
        {
            return View();
        }
        // Xử lý thêm sản phẩm mới
        
        [HttpPost]
        public async Task<IActionResult> Add(Style style)
        {
            if (style == null)
            {
                return BadRequest("Category object is null");
            }

            if (ModelState.IsValid)
            {
                await _styleRepository.AddAsync(style);
                return RedirectToAction(nameof(Index));
            }
            return View(style);
        }


        // Hiển thị thông tin chi tiết sản phẩm
       
        public async Task<IActionResult> Display(int id)
        {
            var product = await _styleRepository.GetByIdAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }
        // Hiển thị form cập nhật sản phẩm

        public async Task<IActionResult> Update(int id)
        {
            var style = await _styleRepository.GetByIdAsync(id);
            if (style == null)
            {
                return NotFound();
            }

            return View(style);
        }
        // Xử lý cập nhật sản phẩm
        [HttpPost]
        public async Task<IActionResult> Update(int id, Style style)
        {
            if (ModelState.IsValid)
            {
                var existingStyle = await _styleRepository.GetByIdAsync(id);
                if (existingStyle == null)
                {
                    return NotFound();
                }

                // Cập nhật các thông tin khác của danh mục
                existingStyle.Name = style.Name;

                await _styleRepository.UpdateAsync(existingStyle);

                return RedirectToAction(nameof(Index));
            }
            return View(style);
        }


        // Hiển thị form xác nhận xóa sản phẩm
        public async Task<IActionResult> Delete(int id)
        {
            var style = await _styleRepository.GetByIdAsync(id);
            if (style == null)
            {
                return NotFound();
            }
            return View(style);
        }

        // Xử lý xóa sản phẩm
        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _styleRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
