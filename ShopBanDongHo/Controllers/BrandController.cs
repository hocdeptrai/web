﻿using ShopBanDongHo.Models;
using ShopBanDongHo.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;


namespace ShopBanDongHo.Controllers
{
    public class BrandController : Controller
    {
        private readonly IBrandRepository _brandRepository;

        public BrandController(IBrandRepository brandRepository)
        {
            _brandRepository = brandRepository;
        }

        // Hiển thị danh sách sản phẩm
        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            var categories = await _brandRepository.GetAllAsync();
            return View(categories);
        }
        // Hiển thị form thêm sản phẩm mới

        public async Task<IActionResult> Add()
        {
            return View();
        }
        // Xử lý thêm sản phẩm mới
        [HttpPost]
        public async Task<IActionResult> Add(Brand brand)
        {
            if (brand == null)
            {
                return BadRequest("Category object is null");
            }

            if (ModelState.IsValid)
            {
                await _brandRepository.AddAsync(brand);
                return RedirectToAction(nameof(Index));
            }
            return View(brand);
        }

        public async Task<IActionResult> Update(int id)
        {
            var brand = await _brandRepository.GetByIdAsync(id);
            if (brand == null)
            {
                return NotFound();
            }

            return View(brand);
        }
        // Xử lý cập nhật sản phẩm
       
        [HttpPost]
        public async Task<IActionResult> Update(int id, Brand brand)
        {
            if (ModelState.IsValid)
            {
                var existingBrand = await _brandRepository.GetByIdAsync(id);
                if (existingBrand == null)
                {
                    return NotFound();
                }

                // Cập nhật các thông tin khác của danh mục
                existingBrand.Name = brand.Name;

                await _brandRepository.UpdateAsync(existingBrand);

                return RedirectToAction(nameof(Index));
            }
            return View(brand);
        }


        // Hiển thị form xác nhận xóa sản phẩm
        public async Task<IActionResult> Delete(int id)
        {
            var category = await _brandRepository.GetByIdAsync(id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }

        // Xử lý xóa sản phẩm
        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _brandRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
