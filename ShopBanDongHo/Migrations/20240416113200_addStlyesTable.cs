﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ShopBanDongHo.Migrations
{
    /// <inheritdoc />
    public partial class addStlyesTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Style_StyleId",
                table: "Products");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Style",
                table: "Style");

            migrationBuilder.RenameTable(
                name: "Style",
                newName: "Styles");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Styles",
                table: "Styles",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Styles_StyleId",
                table: "Products",
                column: "StyleId",
                principalTable: "Styles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Styles_StyleId",
                table: "Products");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Styles",
                table: "Styles");

            migrationBuilder.RenameTable(
                name: "Styles",
                newName: "Style");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Style",
                table: "Style",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Style_StyleId",
                table: "Products",
                column: "StyleId",
                principalTable: "Style",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
